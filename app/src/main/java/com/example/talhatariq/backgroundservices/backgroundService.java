package com.example.talhatariq.backgroundservices;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class backgroundService extends Service {
    MediaPlayer music;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        music=MediaPlayer.create(this,R.raw.music);
        music.setLooping(false);
        music.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        music.stop();
        music.release();
    }
}
